
function balancedpayments_pane_error( pane_selector, error_list ) {
   // Create the error element.
   jQuery('.bp-errors').fadeOut().empty();
   jQuery('.bp-errors').append(
      '<p>Missing or incorrectly filled out fields are highlighted below.</p>'
   );

   // Clear any existing errors and highlight the current ones.
   jQuery(pane_selector + ' .error').removeClass( 'error' );
   jQuery.each( error_list, function( key, value ) {
      if( 'expiry' == key ) {
         return;
      }
      jQuery(pane_selector + ' .bp-errors').append(
         '<li>' + value + '</li>'
      );
      jQuery(pane_selector + ' .bp_' + key).addClass( 'error' );
   } );

   jQuery(pane_selector + ' .bp-errors').fadeIn();

   // Scroll to the payment pane.
   jQuery('html, body').animate( {
      scrollTop: jQuery(pane_selector).offset().top,
   }, 1000 );
}

