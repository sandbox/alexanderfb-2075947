
jQuery(document).ready( function() {

   balanced.init(
      Drupal.settings.balancedPayments.mpuri
   );

   jQuery('#user-profile-form').submit( function( event ) {

      // TODO: Disable 
      //jQuery( '#edit-submit' ).attr( 'disabled', 'disabled' );

      // Don't bother tokenizing if the user isn't attempting to make a bank
      // account entry.
      if(
         '1' == jQuery('#edit-bp-ba-filled').val() ||
         (!jQuery('#edit-bp-account-num').val() &&
         !jQuery('#edit-bp-routing-num').val())
      ) {
         jQuery('#user-profile-form').get( 0 ).submit();
         return;
      }

      // Make sure the user agrees to the terms and conditions.
      if( !jQuery('#edit-bp-ba-accept').attr( 'checked' ) ) {
         balancedpayments_pane_error(
            '#edit-balancedpayments-merchant .fieldset-wrapper',
            { 
               'terms_and_conditions':
               'You must accept the terms and conditions.'
            }
         );
         return false;
      }

      // Make sure the user has entered an EIN.
      if( !jQuery('#edit-bp-ba-ein').val() ) {
         balancedpayments_pane_error(
            '#edit-balancedpayments-merchant .fieldset-wrapper',
            { 'ba_ein': 'Missing field' }
         );
         return false;
      }

      // Tokenize!
      var baData = {
         'account_number': jQuery('#edit-bp-account-num').val(),
         'name': jQuery('#edit-bp-real-name').val(),
         'bank_code': jQuery('#edit-bp-routing-num').val(),
         // TODO: Implement select list for account type.
         'type': 'checking',
      };
      balanced.bankAccount.create( baData, function( response ) {
         switch( response.status ) {
            case 201:
               // Card approved.
               jQuery('#edit-bp-ba-uri').val( response.data['uri'] );
               jQuery('#user-profile-form').get( 0 ).submit();
               return;

            default:
               balancedpayments_pane_error(
                  '#edit-balancedpayments-merchant .fieldset-wrapper',
                  response.error
               );
               return;
         }
      } );

      return false;
   } );
} );

